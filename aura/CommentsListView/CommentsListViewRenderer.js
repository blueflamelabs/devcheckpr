({
		 rerender : function(cmp,helper){
         var table = cmp.find("tableContent").getElement();
         var columns = cmp.get("v.columns");
         
         if(cmp.get("v.initializeDone")){
             window.setTimeout(function(){
                 if(cmp.isValid()) {
                     var resize = new columnResizer(table, {config:columns});
                     resize.initialize();  
                 }
             },0);
         }   
         
     },
})
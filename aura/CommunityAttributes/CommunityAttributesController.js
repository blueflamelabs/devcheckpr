({
	handleApplicationEvent : function(component, event) 
    {
    	var varCaseId = event.getParam("CaseId");
		var varCaseNumber = event.getParam("CaseNumber");
    	// set the handler attributes based on event data
    	component.set("v.CaseRecordId", varCaseId);
		component.set("v.CaseNumber", varCaseNumber);
	}
})
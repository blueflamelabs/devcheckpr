({
    doInit : function(component, event, helper) {
        var file = component.get("v.row");
        console.log(component.get("v.row").contentSize);
        component.set("v.contentSize",helper.bytesToSize(component.get("v.row").contentSize));
        
        /*070619-T-00185-to get and display the file type of the file */
        var icon;
        switch(file.fileType){
            case "PDF":
                icon="doctype:pdf";
                break;
			case "TXT":
                icon="doctype:txt";
                break;
            case "JPG":
                icon="doctype:image";
                break;
            case "JPEG":
                icon="doctype:image";ent
                break;
            case "PNG":
                icon="doctype:image";
                break;
            case "WORD":
                icon="doctype:word";
                break;
            case "WORD_X":
                icon="doctype:word";
                break;
            case "ZIP":
                icon="doctype:zip";
                break;
            case "EXCEL":
                icon="doctype:excel";
                break;
            case "EXCEL_X":
                icon="doctype:excel";
                break;
            case "CSV":
                icon="doctype:csv";
                break;
            case "POWER_POINT":
                icon="doctype:ppt";
                break;
            case "POWER_POINT_X":
                icon="doctype:ppt";
                break;
            case "RTF":
                icon="doctype:rtf";
                break;
            default:
                icon="doctype:attachment";
        }
        component.set("v.icon",icon);
        // 130619 - T - 00185 - to get the current url if it is blank that will internal user url.
        helper.getCurrentUrl(component, event, helper);
        helper.fileReviewForUser(component, event, helper);
    },
    
    onCheckChange:function(component,event,helper){
        //component.set("v.isEdited",true);  
        let caseMoniterRecord = {},
            row = component.get("v.row");
        caseMoniterRecord.Id = row.caseMoniterId;
        caseMoniterRecord.Review_Complete__c = row.fileReviewd;
        
        let action = component.get("c.saveRecordCmRecord");
        
        action.setParams({'cmRecord':JSON.stringify(caseMoniterRecord)});
        
        action.setCallback(this,(response)=>{
            const state = response.getState();
            console.log(state,response.getReturnValue());
            if(state === 'SUCCESS'){
            //helper.hideSpinner(component, event, helper);
            
        } else if(state === 'ERROR'){
            //helper.hideSpinner(component, event, helper);
        }
        
    });
    
    $A.enqueueAction(action);         
},
 
 // Method to open the popup for confirmation the file deletion.
 handleClick : function(component, event, helper){
    
    component.set("v.deleteFile",true);
    var rowId = component.get("v.row.fileId");
    alert("rowid : "+rowId);
    
},
    handleSelect: function (component, event, helper) {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem
        var selectedMenuItemValue = event.getParam("value");
        //alert("Menu item selected with value: " + selectedMenuItemValue);
        var modalBody;
        if(selectedMenuItemValue === 'delete'){
            //component.set("v.deleteFile",true);
            
            $A.createComponent("c:fileDelete", { fileIds : component.get("v.row.fileId")},
                                function(component,status) {
                                    if (component.isValid()) {
                                        if(status == "SUCCESS") {
                                            var body = component.get("v.body");
                                            body.push(component);
                                            component.find('overlayLib').showCustomModal({
                                                header: "Delete Files?",
                                                body : body,
                                                showCloseButton: true
                                            });
                                            }
                                    }
                                });
            
            //helper.deleteSelectedFile(component, event, helper, rowId);
        }/*else if(selectedMenuItemValue === 'editFileDetails') {
        console.log('testedit---');
        helper.editRecord(component, event, helper);
    }else if(selectedMenuItemValue == 'uploadnewVersion'){
        
        
    }else if(selectedMenuItemValue == 'viewFileDetails') {
        console.log('testfiledetail---');
        var navigationSObject = $A.get("e.force:navigateToSObject");
        console.log('navigate--',navigationSObject);
        navigationSObject.setParams({
            "recordId": component.get("v.row.fileId")
        });
        navigationSObject.fire();
    }*/
    },
        // 070619 - T - 00185 - Method to Preview the file.
        navigateToPreview :function(component,event,helper){
    	 	$A.get('e.lightning:openFiles').fire({
				recordIds: [component.get("v.row.fileId")]
            });
            
        },
        
        // 060619 - T - 00185 - Method to redirect to the Owner of the file record detail page .
        navigateToOwnRecord :function(component,event,helper){
            var navigationSObject = $A.get("e.force:navigateToSObject");
            console.log('navigate--',navigationSObject);
            navigationSObject.setParams({
                "recordId": component.get("v.row.ownerId")
            });
            navigationSObject.fire();
        },
            
        // 060619 - T - 00185 - Method to redirect to the file's parent detail page .
        navigateToParentRecord :function(component,event,helper){
        	var navigationSObject = $A.get("e.force:navigateToSObject");
                console.log('navigate--',navigationSObject);
                navigationSObject.setParams({
                 "recordId": component.get("v.row.parentId")
                });
                navigationSObject.fire();
        },  
                
		// 060619 - T - 00185 - to close the Popup on close button.
        closeDeleteModel : function(component, event, helper){
           component.set("v.deleteFile",false);
        },
            
        deleteSelectedFile:function(component, event, helper){
           helper.deleteSelFile(component, event, helper);     
        }
      
})
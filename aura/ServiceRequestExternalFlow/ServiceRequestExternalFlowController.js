({
	myAction : function(component, event, helper) 
    {
		
	},
    init : function(component, event, helper) 
    {
    	helper.GetUserInfo(component);
        
    },
    
    handleStatusChange : function (component, event) 
    {
      
        
        if(event.getParam("status") === "FINISHED") 
        {
                        
            // Get the output variables and iterate over them
            var outputVariables = event.getParam("outputVariables");
            var outputVar;
            for(var i = 0; i < outputVariables.length; i++) 
            {
                outputVar = outputVariables[i];
                // Pass the values to the component's attributes
                if(outputVar.name == "CaseSubject") 
                {
                    component.set("v.Subject", outputVar.value);
                } 
                if(outputVar.name == "CaseDescription") 
                {
                    component.set("v.Description", outputVar.value);
                } 
                if(outputVar.name == "CaseServiceArea") 
                {
                    component.set("v.ServiceArea", outputVar.value);
                } 
                if(outputVar.name == "CaseCategory") 
                {
                    component.set("v.Category", outputVar.value);
                } 
                if(outputVar.name == "CaseSubCategory") 
                {
                    component.set("v.SubCategory", outputVar.value);
                } 
                if(outputVar.name == "CasePriority") 
                {
                    component.set("v.Priority", outputVar.value);
                } 
                if(outputVar.name == "Electronic_Check_Collection_Batch") 
                {
                    component.set("v.objBatch", outputVar.value);
                    //alert(JSON.stringify(outputVar.value))
                } 
                if(outputVar.name == "Electronic_Check_Collection_Batch_Item") 
                {
                    component.set("v.objBatchItem", outputVar.value);
                }
                if(outputVar.name == "AdjustmentRecord") 
                {
                    component.set("v.objAdjustment", outputVar.value);
                    //alert(JSON.stringify(outputVar.value))
                } 
                if(outputVar.name == "ManualRecord") 
                {
                    component.set("v.objManual", outputVar.value);
                } 

                if(outputVar.name == "varContactId") 
                {
                    component.set("v.ContactId", outputVar.value);
                } 
                
            }
            
            var strSubject = component.get("v.Subject");
            var strDescription = component.get("v.Description");
            var strServiceArea = component.get("v.ServiceArea");
            var strCategory = component.get("v.Category");
            var strSubCategory = component.get("v.SubCategory");
            var strPriority = component.get("v.Priority");
            
            // fix
            var strRequestorName = 'Community';
            
            var iCaseId;
            var varCase = component.get("v.objCase");
            var varCaseId = component.get("v.CaseRecordId");                   
            var varContactId = component.get("v.ContactId");                   
            
            if ((strServiceArea == 'Check Processing')&&(strCategory == 'Batch-Item Deletion-Correction'))
            {
                
                var varBatch = component.get("v.objBatch");
                var varBatchItem = component.get("v.objBatchItem");
                var varBatchItemArray = component.get("v.objBatchItemArray");       
                var newItem = JSON.parse(JSON.stringify(varBatchItem));
                
                varBatchItemArray.push(newItem);
                component.set("v.objBatchItemArray", varBatchItemArray);
                
                // Save Case here first 
                var actionSaveCase = component.get("c.SaveECCCase");
                
                actionSaveCase.setParams(
                    {
                        "strSubject" : strSubject,
                        "strDescription" : strDescription,
                        "strServiceArea" : strServiceArea,
                        "strCategory" : strCategory,
                        "strSubCategory" : strSubCategory,
                        "strPriority" : strPriority,
                        "obj": JSON.stringify(varBatch),
                        "objItemList" : JSON.stringify(varBatchItemArray),
                        "objContactId" : varContactId
                        
                    });
                
                actionSaveCase.setCallback(this, function(response)
               {
                                               var state = response.getState();
                                               if (state === "SUCCESS") 
                                               {
                                                   //get the return object 
                                                   var objCase = component.get("v.objCase");
                                                   component.set("v.CaseRecordId", response.getReturnValue());
                                                   
                                                   var objCaseId = component.get("v.CaseRecordId");

  													var urlEvent = $A.get("e.force:navigateToSObject");
                                                   urlEvent.setParams(
                                                       {
                                                            "recordId": objCaseId,
                                                           "isredirect": true
                                                      });
                                                   urlEvent.fire();
                                               }
                                               else if (state === "ERROR") 
                                               {
                                                   var err = response.getError();
                                                   
                                                   let toastParams = {
                                                        title: "Error!",
                                                        message: "Unknown error", // Default error message
                                                        "mode": "sticky",
                                                       type: "error"
                                                    };
                                                    // Pass the error message if any
                                                    if (err && Array.isArray(err) && err.length > 0) {
                                                        toastParams.message = err[0].message;
                                                    }
                                                    // Fire error toast
                                                    let toastEvent = $A.get("e.force:showToast");
                                                    toastEvent.setParams(toastParams);
                                                    toastEvent.fire();
                                               }
                                           });
                $A.enqueueAction(actionSaveCase);
            }
            else if ((strServiceArea == 'Check Processing')&&(strCategory == 'Adjustment'))
            {
                 var varAdjustment = component.get("v.objAdjustment");

                // Save Case here first 
                var actionSaveCase = component.get("c.SaveAdjustmentCase");
                
                actionSaveCase.setParams(
                    {
                        "strSubject" : strSubject,
                        "strDescription" : strDescription,
                        "strServiceArea" : strServiceArea,
                        "strCategory" : strCategory,
                        "strSubCategory" : strSubCategory,
                        "strPriority" : strPriority,
                        "obj": JSON.stringify(varAdjustment),
                        "objContactId" : varContactId
                        
                    });
                
                actionSaveCase.setCallback(this, function(response)
               {
                                               var state = response.getState();
                                               if (state === "SUCCESS") 
                                               {
                                                   component.set("v.CaseRecordId", response.getReturnValue());
                                                   
                                                   var objCaseId = component.get("v.CaseRecordId");

  													var urlEvent = $A.get("e.force:navigateToSObject");
                                                   urlEvent.setParams(
                                                       {
                                                            "recordId": objCaseId,
                                                           "isredirect": true
                                                      });
                                                   urlEvent.fire();
                                               }
                                               else if (state === "ERROR") 
                                               {
                                                   var err = response.getError();
                                                   
                                                   let toastParams = {
                                                        title: "Error!",
                                                        message: "Unknown error", // Default error message
                                                        "mode": "sticky",
                                                       type: "error"
                                                    };
                                                    // Pass the error message if any
                                                    if (err && Array.isArray(err) && err.length > 0) {
                                                        toastParams.message = err[0].message;
                                                    }
                                                    // Fire error toast
                                                    let toastEvent = $A.get("e.force:showToast");
                                                    toastEvent.setParams(toastParams);
                                                    toastEvent.fire();
                                               }
                                           });
                $A.enqueueAction(actionSaveCase);
                
                
            }
            else if ((strServiceArea == 'Check Processing')&&(strCategory == 'Manual'))
            {
                //alert('In Manuals.');
                var varManual = component.get("v.objManual");

                // Save Case here first 
                var actionSaveCase = component.get("c.SaveManualCase");
                //alert('setParams'); 
                actionSaveCase.setParams(
                    {
                        "strSubject" : strSubject,
                        "strDescription" : strDescription,
                        "strServiceArea" : strServiceArea,
                        "strCategory" : strCategory,
                        "strSubCategory" : strSubCategory,
                        "strPriority" : strPriority,
                        "obj": JSON.stringify(varManual),
                        "objContactId" : varContactId
                        
                    });
                //alert('setCallback.'); 
                actionSaveCase.setCallback(this, function(response)
               {
                                               var state = response.getState();
                                               if (state === "SUCCESS") 
                                               {
                                                   component.set("v.CaseRecordId", response.getReturnValue());
                                                   var objCaseId = component.get("v.CaseRecordId");

  													var urlEvent = $A.get("e.force:navigateToSObject");
                                                   urlEvent.setParams(
                                                       {
                                                            "recordId": objCaseId,
                                                           "isredirect": true
                                                      });
                                                   urlEvent.fire();
                                               }
                                               else if (state === "ERROR") 
                                               {
                                                   var err = response.getError();
                                                   
                                                   let toastParams = {
                                                        title: "Error!",
                                                        message: "Unknown error", // Default error message
                                                        "mode": "sticky",
                                                       type: "error"
                                                    };
                                                    // Pass the error message if any
                                                    if (err && Array.isArray(err) && err.length > 0) {
                                                        toastParams.message = err[0].message;
                                                    }
                                                    // Fire error toast
                                                    let toastEvent = $A.get("e.force:showToast");
                                                    toastEvent.setParams(toastParams);
                                                    toastEvent.fire();
                                               }
                                           });
                $A.enqueueAction(actionSaveCase);
                
            }
            else
            {

                // Save Case here first 
                var actionSaveCase = component.get("c.SaveDefaultCase");
                //alert('setParams'); 
                actionSaveCase.setParams(
                    {
                        "strSubject" : strSubject,
                        "strDescription" : strDescription,
                        "strServiceArea" : strServiceArea,
                        "strCategory" : strCategory,
                        "strSubCategory" : strSubCategory,
                        "strPriority" : strPriority,
                        "objContactId" : varContactId
                        
                    });
                //alert('setCallback.'); 
                actionSaveCase.setCallback(this, function(response)
               {
                                               var state = response.getState();
                                               if (state === "SUCCESS") 
                                               {
                                                   component.set("v.CaseRecordId", response.getReturnValue());
                                                   var objCaseId = component.get("v.CaseRecordId");

  													var urlEvent = $A.get("e.force:navigateToSObject");
                                                   urlEvent.setParams(
                                                       {
                                                            "recordId": objCaseId,
                                                           "isredirect": true
                                                      });
                                                   urlEvent.fire();
                                               }
                                               else if (state === "ERROR") 
                                               {
                                                   var err = response.getError();
                                                   
                                                   let toastParams = {
                                                        title: "Error!",
                                                        message: "Unknown error", // Default error message
                                                        "mode": "sticky",
                                                       type: "error"
                                                    };
                                                    // Pass the error message if any
                                                    if (err && Array.isArray(err) && err.length > 0) {
                                                        toastParams.message = err[0].message;
                                                    }
                                                    // Fire error toast
                                                    let toastEvent = $A.get("e.force:showToast");
                                                    toastEvent.setParams(toastParams);
                                                    toastEvent.fire();
                                               }
                                           });
                //alert('enqueueAction.'); 
                $A.enqueueAction(actionSaveCase);
    
            }
            
            
            
        }
   },
    
})
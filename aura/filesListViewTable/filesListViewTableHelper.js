({
    //timer : null,
    doInit : function(component,event) {
        console.log('---call first-11-');
        let action = component.get("c.getFilesRecords");
        
        action.setParams({'parentId':component.get("v.parentId")});
        
        action.setCallback(this,(response) => {
            const state = response.getState();
            if(state === 'SUCCESS') {
            component.set("v.rowsToDisplay",response.getReturnValue());
            var totalfiles = response.getReturnValue();
            if(totalfiles.length > 50){
                var fileSize = '50' + '+';
                component.set("v.totalfiles",fileSize);
        	}else{         
              	component.set("v.totalfiles",totalfiles.length);             
    		}
        }
                           
                           });
        $A.enqueueAction(action);
        
    },
    setColumnWrapper: function(component,event) {
        console.log('---call 2nd--');
        //070619-T-00185-Change the label In CAPS.
        var displayfilePrivacy = component.get("v.displayColumn");
        console.log('--displayfilePrivacy--',displayfilePrivacy);
        if(displayfilePrivacy){
            const columns = [
                {
                    'label' : 'TITLE', 
                    'apiName' : 'title',
                    'type':'reference'
                },
                {
                    'label' : 'OWNER',
                    'apiName' : 'ownerName',
                    'type':'reference'
                },
                {
                    'label' : 'LAST MODIFIED',
                    'apiName' : 'lastModified',
                    'type':'string'
                },
                {
                    'label' : 'SIZE',
                    'apiName' : 'contentSize',
                    'type':'string'
                },
                /*120619-T-00185-to remove from the table column*/
                
                {
                    'label' : 'SHARING',
                    'apiName' : 'filePrivacy',
                    'type':'string'
                },
                {
                    'label' : 'FILE REVIEWED',
                    'apiName' : 'fileReviewd',
                    'type':'boolean'
                },
                
                
            ];
                component.set("v.columns",columns);
                }
                else {
                
                const columns = [
                {
                'label' : 'TITLE', 
                'apiName' : 'title',
                'type':'reference'
                },
                {
                'label' : 'OWNER',
                'apiName' : 'ownerName',
                'type':'reference'
                },
                {
                'label' : 'LAST MODIFIED',
                'apiName' : 'lastModified',
                'type':'string'
                },
                {
                'label' : 'SIZE',
                'apiName' : 'contentSize',
                'type':'string'
                },
                
                /*{
                'label' : 'FILE REVIEWED',
                'apiName' : 'fileReviewd',
                'type':'boolean'
            },*/
            
            
        ];
        
        component.set("v.columns",columns);
    }
     
 },
    sortBy: function(component,event) {
        let sortProperties = component.get("v.sortProperties"),
            records = component.get("v.rowsToDisplay"),
            sortAsc = sortProperties.sortOrder == 'asc' ? true : false,
            field = sortProperties.sortField;
        console.log('--field--',field);
        
        records.sort(function(a,b){
            let avalue = (a[field] != 0 && a[field])  ? a[field] : null;
            let bvalue = (b[field] != 0 && b[field]) ? b[field] : null;
            
            if(avalue == "" || avalue == null) return 1;
            if(bvalue == "" || bvalue == null) return -1;
            
            if(field == 'OpportunityTotal' || field == 'MinimumDriverCount' || field == 'actualPriceIncrease'){
                avalue = (avalue != 0 && avalue) ? parseInt(avalue) : null; 
                bvalue = (bvalue != 0 && bvalue) ? parseInt(bvalue) : null;
            }
            console.log(avalue,bvalue);
            
            let t1 = avalue == bvalue ,
                t2 = (avalue != 0 && !avalue && bvalue) || (avalue < bvalue);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        
        component.set("v.rowsToDisplay", records);
        
    }, 
    
    startTimer : function(component, event, helper) {
        
        if(this.timer){
            return;
        }
        //component.set("v.displaytimer",false);
        var secondsLabel = component.find("demo");
        var minutesLabel = component.find("demo");
        
        var resettimer = component.find("colCmp");
        //console.log('-secondsLabel-',secondsLabel);
        var totalSeconds = 0;
        this.timer = setInterval(setTime, 1000);
        
        function setTime() {
            ++totalSeconds;
            
            secondsLabel.innerHTML = pad(totalSeconds % 60);
            //console.log('innerhtml--'+secondsLabel.innerHTML);
            
            minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
            var minuteVal = minutesLabel.innerHTML;
            //console.log('innerhtml--'+minuteVal);
            
            var hourVal = minuteVal / 60 ;
            //console.log('--hour--'+hourVal);
            
            if(secondsLabel.innerHTML < 59){
                var seconds = 'a few seconds';
                component.set("v.seconds", seconds);
            }
            if(minuteVal >= 1){
                var minutes = minutesLabel.innerHTML + ' minutes';
                component.set("v.seconds", minutes);
            }
            if(hourVal >= 1){
                component.set("v.seconds", hourVal);
            }
        }
        
        
        function pad(val) {
            var valString = val + "";
            /*if (valString.length < 2) {
                    return "0" + valString;
                } else {
                    return valString;
                }*/
                return valString;
            }
        },
    
    
    
})
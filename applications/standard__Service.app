<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-Solution</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Knowledge__kav</tab>
    <tab>Contract__c</tab>
    <tab>Change_Management_Phase__c</tab>
    <tab>standard-WaveHome</tab>
    <tab>Case_Monitoring__c</tab>
    <tab>Custom_Content_Document_Link__c</tab>
</CustomApplication>

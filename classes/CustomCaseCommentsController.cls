public class CustomCaseCommentsController 
{
  
    
     public Class CustomCaseCommentsWrapper 
     {
       
        @AuraEnabled
        public ID createById;
        @AuraEnabled
        public String createByName;
        @AuraEnabled
        public ID CaseCommentId;
        @AuraEnabled
        public String CommentBody;
        @AuraEnabled
        public DateTime CreatedDate;
        @AuraEnabled
        public boolean PublicFlag;
        @AuraEnabled
        public boolean CommentReviewComplete;
        @AuraEnabled
        public String createByIdHREF;
        @AuraEnabled
        public boolean IsCommunityUser;
        @AuraEnabled
        public String source;
        @AuraEnabled
        public String Name;
        @AuraEnabled
        public String CaseNumber;
        @AuraEnabled
        public boolean IsCommunityComment;
        @AuraEnabled
        public String ownerId;


    } 

    @AuraEnabled
    public static List<syn_Case_Comment__c> getCaseComments(String caseId) 
    {
        
        List<syn_Case_Comment__c> caseComments = 
                [SELECT Id, Name, Parent_ID__c, IsPublished__c,CommentBody__c,  Review_Complete__c, CreatedDate, CreatedBy.Name, LastModifiedById FROM syn_Case_Comment__c 
                 where Case__c=:caseId order by CreatedDate desc];

        return caseComments;        
    }


    @AuraEnabled
    public static List<CustomCaseCommentsWrapper> GetCaseCommentCaseMonitoring(String caseId) 
    {
        boolean bCommUser = GetIsCommunityUser (); 
        List<CustomCaseCommentsWrapper> objCustomCaseCommentsWrapperList = new  List<CustomCaseCommentsWrapper>();
        List<syn_Case_Comment__c> objCaseCommentsList;
        if (bCommUser)
        {
            // List<syn_Case_Comment__c> objCaseCommentsList = 
            objCaseCommentsList = 
                [SELECT Id, Name, Parent_ID__c, IsPublished__c, CommentBody__c, Review_Complete__c, CreatedDate, CreatedBy.Name, LastModifiedById, CreatedBy.Id FROM syn_Case_Comment__c 
                 where Case__c=:caseId and IsPublished__c = true order by CreatedDate desc];
        }
        else
        {
               objCaseCommentsList = 
                [SELECT Id, Name, Parent_ID__c, IsPublished__c, CommentBody__c, Review_Complete__c, CreatedDate, CreatedBy.Name, LastModifiedById, CreatedBy.Id FROM syn_Case_Comment__c 
                 where Case__c=:caseId  order by CreatedDate desc];
     
        }
        
        Case objCase =  [SELECT Id, CaseNumber, OwnerId FROM Case where ID=:caseId ];
        
        
        for (syn_Case_Comment__c objComment : objCaseCommentsList)
        {
            boolean bAdd = true;
            boolean bIsCommunityComment = true;
            boolean bIspub = objComment.IsPublished__c;
            if (bCommUser)
            {
                if (bIspub == false)
                {
                    bAdd = false;    
                }
                else
                {
                    bAdd = true;    
                }
            }
            /****************************************************/
            /*  Determine if comment author is a community user */
            /****************************************************/
            bIsCommunityComment = getUserOfCaseComment(objComment.CreatedBy.Id);
            if (bAdd == true)
            {
                CustomCaseCommentsWrapper objWrapper = new CustomCaseCommentsWrapper();
                //objWrapper.objCaseComment = objComment;
                objWrapper.CaseCommentId = objComment.ID;
                objWrapper.CommentBody = objComment.CommentBody__c;
                objWrapper.CreatedDate = objComment.CreatedDate;
                objWrapper.PublicFlag = objComment.IsPublished__c;
                objWrapper.CommentReviewComplete = objComment.Review_Complete__c;
                
                objWrapper.createByName = objComment.CreatedBy.Name;
                objWrapper.createById = objComment.CreatedById;
                objWrapper.createByIdHREF = '/lightning/r/' + objComment.CreatedById + '/view';
                
                objWrapper.IsCommunityUser  = GetIsCommunityUser ();
                objWrapper.IsCommunityComment  = bIsCommunityComment;
                objWrapper.CaseNumber = objCase.CaseNumber;
                objWrapper.Name  = objComment.Name;
                objWrapper.ownerId = objCase.OwnerId;

                    //add to list
                    objCustomCaseCommentsWrapperList.add(objWrapper);
            }
        }
        // return list
        return objCustomCaseCommentsWrapperList;        
    }

    @AuraEnabled
    public static boolean GetIsCommunityUser () 
    {
        //
        return (getPathPrefix() != '' ? true : false);
        //
        
    }

    
    @AuraEnabled
    public static String getPathPrefix () {
        Id net_id = Network.getNetworkId();
        if ( net_id  != null) {
            return [SELECT Id, UrlPathPrefix
                    FROM Network
                    WHERE Id = :net_id].UrlPathPrefix;
        }
        return '';
    } 

    @AuraEnabled
    public static syn_Case_Comment__c addCaseComment(String caseId, String commentBody, boolean published) 
    {
        //system.debug('in addcasecomment');
        
        Case objCase =  [SELECT Id, CaseNumber FROM Case where ID=:caseId ];
  
        
        syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
        
        caseComment.Parent_ID__c=caseId;
        caseComment.Case__c=caseId;
        caseComment.CommentBody__c=commentBody;
        //caseComment.IsPublished__c = published;
        caseComment.Name = 'Case:' + objCase.CaseNumber + ' Case Comment';
        //caseComment.OwnerId = 
        
        // add logic here to turn off on internal
        // 
        boolean bIsComm = GetIsCommunityUser();
        if (bIsComm)
        {
            caseComment.Review_Complete__c = false;
            caseComment.IsPublished__c = true;   
        }
        else
        {
            caseComment.Review_Complete__c = true; 
            caseComment.IsPublished__c = published;             
        }
       
        
        insert caseComment;
        
        //system.debug('exiting addcasecomment');
        
        return caseComment;        
    }

    @AuraEnabled
    public static syn_Case_Comment__c EditCaseComment(String casecommentId, String commentBody, boolean published, boolean bReviewComplete) 
    {
        //system.debug('in EditCaseComment');
        
        syn_Case_Comment__c objCaseComment =  [SELECT Id, CommentBody__c, IsPublished__c, Review_Complete__c FROM syn_Case_Comment__c where ID=:casecommentId ];
  
        
        
        objCaseComment.CommentBody__c=commentBody;
        objCaseComment.IsPublished__c = published;
        

        objCaseComment.Review_Complete__c = bReviewComplete; 
        
        update objCaseComment;
        
        //system.debug('exiting EditCaseComment');
        
        return objCaseComment;        
    }



    @AuraEnabled
    public static void UpdateReviewCompleteMonitoring(String CaseCommentId, Boolean bReviewComplete, string strCommunicationType) 
    {
        
        syn_Case_Comment__c objCaseComment =  [SELECT Id, CommentBody__c, IsPublished__c, Review_Complete__c FROM syn_Case_Comment__c where ID=:CaseCommentId ];
          
        
        // add logic here to turn off on internal
        objCaseComment.Review_Complete__c = bReviewComplete; 
        
        update objCaseComment;
        
        
        List<Case_Monitoring__c> objList =  [SELECT Id, Parent_ID__c, Review_Complete__c, Communication_Type__c, Acknowledge_Date__c  FROM Case_Monitoring__c  where Parent_ID__c=:CaseCommentId and Communication_Type__c=:strCommunicationType];
        
        for (Case_Monitoring__c obj : objList)
        {
            obj.Review_Complete__c =  bReviewComplete;
            // 280619 - T - 00379 - Commented the code to set Acknowledge Date as this is handled by Process Builder
            /*
            if (bReviewComplete == true)
            {
                obj.Acknowledge_Date__c = System.Now();            
            }
            else
            {
                obj.Acknowledge_Date__c = null;               
            }
            */
        }

        if (objList.size() > 0)
        {
            update objList;    
        }

    }    
 
    @AuraEnabled
    public static void UpdateComment(String CaseCommentId, Boolean bPublished) 
    {
        
        syn_Case_Comment__c objCaseComment =  [SELECT Id, CommentBody__c, IsPublished__c, Review_Complete__c FROM syn_Case_Comment__c where ID=:CaseCommentId ];
          
        
        // add logic here to turn off on internal
        objCaseComment.IsPublished__c = bPublished; 
        
        update objCaseComment;
        
    }    
    
    @AuraEnabled
    public static Boolean getUserOfCaseComment(String createdbyId){
        //System.debug('==caseOwnerName=='+caseOwnerName);
       
        User caseCommentUser = [ SELECT Id,
                                        Name,
                                        Email,
                                        FirstName,
                                        LastName,
                                        IsActive,
                                        IsPortalEnabled,
                                        ContactId 
                                   FROM User 
                                  WHERE Id = :createdbyId
                               ];
        System.debug('==caseCommentUser=='+caseCommentUser);
        if(caseCommentUser.ContactId != null){
            return true;
        }else{
            return false;
        }
    }
    

}
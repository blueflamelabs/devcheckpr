/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*     1.0        180619         BFL Users     This class is built to test CaseCommentTrigger apex trigger
**********************************************************************************************************************************************************/ 
@isTest
public class CaseCommentTrigger_Test {
    
    /**
    * Method Name : positiveTestAfterInsert
    * Parameters  : none
    * Description : This method is used to test positive scenario of send mail functionality for internal users.
    **/
    @isTest
    public static void positiveTestAfterInsert() {
        
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        //Id p = [select id from profile where name='+Synergent Power Community User'].id;
        System.runAs(portalAccountOwner1) {
            test.startTest();
            
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = true;
            custObj.Is_File_Notification_Active__c = false;
            
            insert custObj;
            
            Account accObj = new Account();
            accObj.Name = 'Test Account';
            
            insert accObj;
            
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact';
            objContact.AccountId = accObj.Id;
            objContact.Email = 'test@gmail.com';
            
            insert objContact;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subject';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.ContactId = objContact.Id;
            caseObj.recordtypeid = RecordTypeIdCase;
            
            insert caseObj;
            
            
            
            List<syn_Case_Comment__c> caseCommentList = new List<syn_Case_Comment__c>();
            for(Integer i=0; i< 5; i++){
                syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
                caseComment.Name = 'Test'+i ;
                caseComment.IsPublished__c = true;
                caseComment.Case__c = caseObj.Id;
                caseCommentList.add(caseComment);
            }
            insert caseCommentList;
            
            CaseCommentHandler.Bypasstrigger = true;
            
            Test.stopTest();
            
            system.assertEquals(5, caseCommentList.size());
            
        }
        
        
    }
    
    /**
    * Method Name : positiveTestAfterInsert1
    * Parameters  : none
    * Description : This method is used to test positive scenario of send mail functionality for community users.
    **/
    @isTest
    public static void positiveTestAfterInsert1() {
        
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        Id p = [select id from profile where name='+Synergent Power Community User'].id;
        System.runAs(portalAccountOwner1) {
            
            ac = new Account(name ='Grazitti', OwnerId = portalAccountOwner1.Id) ;
            insert ac; 
            
            con = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert con;
            
            user = new User(alias = 'test123', email='test123@noemail.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                            ContactId = con.Id,
                            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user; 
        }
        System.runAs(user) {
            
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = true;
            custObj.Is_File_Notification_Active__c = false;
            
            insert custObj;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subject';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.recordtypeid = RecordTypeIdCase;
            
            insert caseObj;
            
            Test.startTest();
            
            List<syn_Case_Comment__c> caseCommentList = new List<syn_Case_Comment__c>();
            for(Integer i=0; i< 5; i++){
                syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
                caseComment.Name = 'Test'+i ;
                caseComment.IsPublished__c = true;
                caseComment.Case__c = caseObj.Id;
                caseCommentList.add(caseComment);
            }
            insert caseCommentList;
            
            CaseCommentHandler.Bypasstrigger = true;
            Test.stopTest();
            
            system.assertEquals(5, caseCommentList.size());
            
        }
        
        
    }
    
    /**
    * Method Name : positiveTestAfterUpdate
    * Parameters  : none
    * Description : This method is used to test positive scenario of send mail functionality for internal users.
    **/
    @isTest
    public static void positiveTestAfterUpdate() {
        // query for internal user
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        System.runAs(portalAccountOwner1) {
           Test.startTest();
            
            // insert custom setting
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = true;
            custObj.Is_File_Notification_Active__c = false;
            
            insert custObj;
            
            // insert account
            Account accObj = new Account();
            accObj.Name = 'Test Account';
            
            insert accObj;
            
            //insert contact
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact';
            objContact.AccountId = accObj.Id;
            objContact.Email = 'test@gmail.com';
            
            insert objContact;
            
            // insert case
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subject';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.recordtypeid = RecordTypeIdCase;
            caseObj.ContactId = objContact.Id;
            
            insert caseObj;
            
            // insert syn_Case_Comment__c record
            List<syn_Case_Comment__c> caseCommentList = new List<syn_Case_Comment__c>();
            for(Integer i=0; i< 5; i++){
                syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
                caseComment.Name = 'Test'+i ;
                caseComment.IsPublished__c = false;
                caseComment.Case__c = caseObj.Id;
                caseCommentList.add(caseComment);
            }
            insert caseCommentList;

            caseCommentList[0].IsPublished__c = true;
            
            // update syn_Case_Comment__c 
            Update caseCommentList[0];
            
            CaseCommentHandler.Bypasstrigger = true;
            Test.stopTest();
            
            system.assertEquals(5, caseCommentList.size());
            
        }
        
        
    }
    
    /**
    * Method Name : TestAfterUpdateForCommunity
    * Parameters  : none
    * Description : This method is used to test positive scenario of send mail functionality for community users.
    **/
    @isTest
    public static void TestAfterUpdateForCommunity() {
       
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;
        
        
        Id p = [select id from profile where name='+Synergent Power Community User'].id;
        System.runAs(portalAccountOwner1) {
            
            ac = new Account(name ='Grazitti', OwnerId = portalAccountOwner1.Id) ;
            insert ac; 
            
            con = new Contact(LastName ='testCon',AccountId = ac.Id);
            insert con;
            
            user = new User(alias = 'test123', email='test123@noemail.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                            ContactId = con.Id,
                            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user; 
        }
        System.runAs(user) {
         
            Test.startTest();
            // insert Custom setting
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = true;
            custObj.Is_File_Notification_Active__c = false;
            
            insert custObj;
            
            // insert case
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subject';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.recordtypeid = RecordTypeIdCase;
            
            
            insert caseObj;
            
            //insert syn_Case_Comment__c
            List<syn_Case_Comment__c> caseCommentList = new List<syn_Case_Comment__c>();
            for(Integer i=0; i< 5; i++){
                syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
                caseComment.Name = 'Test'+i ;
                caseComment.IsPublished__c = true;
                caseComment.Case__c = caseObj.Id;
                caseCommentList.add(caseComment);
            }
            insert caseCommentList;

            caseCommentList[0].IsPublished__c = true;
            
            // update syn_Case_Comment__c
            Update caseCommentList;
            
            CaseCommentHandler.Bypasstrigger = true;
            Test.stopTest();
            
            system.assertEquals(5, caseCommentList.size());
            
        }
        
    }
    
}
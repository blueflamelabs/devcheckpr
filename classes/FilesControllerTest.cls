/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*     1.0        120619         BFL Users     This class is built to test FilesController apex class
**********************************************************************************************************************************************************/ 
@isTest
public class FilesControllerTest {

    /**
    * Method Name : setupTestData
    * Parameters  : none
    * Description : This method is used to create test data
    **/
    @testSetup
    public static void setupTestData() {
    
        // Variable declarations

        // For account
        Integer accRecCount = 5;
        List<Account> accInsertionList = new List<Account>();
        // For contact
        Integer conRecCount = 5;
        List<Contact> conInsertionList = new List<Contact>();
        // For case
        Integer caseRecCount = 5;
        List<Case> caseInsertionList = new List<Case>();
        // For ContentVersion
        Integer contentVerRecCount = 5;
        List<ContentVersion> contentVerInsertionList = new List<ContentVersion>();
        // For ContentDocumentLink
        Integer contentDocLinkRecCount = 5;
        List<ContentDocumentLink> contentDocLinkInsertionList = new List<ContentDocumentLink>();
        // For Case Monitoring
        Integer caseMonRecCount = 5;
        List<Case_Monitoring__c> caseMonInsertionList = new List<Case_Monitoring__c>();
        
        // Insert Account
        for(Integer i=0; i < accRecCount; i++) {
            Account objAcc = new Account();
            objAcc.Name = 'Test Account'+i;
            accInsertionList.add(objAcc);
        }
        insert accInsertionList;
        
        // Insert Contact
        for(Integer i=0; i < conRecCount; i++) {
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact'+i;
            if(accInsertionList.size() >= i) {
                objContact.AccountId = accInsertionList[i].Id;
            } else if(!accInsertionList.isEmpty()) {
                objContact.AccountId = accInsertionList[0].Id;
            }
            conInsertionList.add(objContact);
        }
        insert conInsertionList;
        
        // Insert Case
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
        //System.debug('RecordTypeIdCase======'+RecordTypeIdCase);
        for(Integer i=0; i < caseRecCount; i++) {
            Case objCase = new Case();
            objCase.Subject = 'Test Subject'+i;
            objCase.Description = 'Test Description'+i;
            objCase.recordtypeid = RecordTypeIdCase;
            objCase.Status = 'New';
            objCase.Origin = 'Email';
            objCase.Date_Required__c = Date.today();
            if(conInsertionList.size() >= i) {
                objCase.ContactId = conInsertionList[i].Id;
                //objCase.AccountId = conInsertionList[i].AccountId;
            } else if(!conInsertionList.isEmpty()) {
                objCase.ContactId = conInsertionList[0].Id;
                //objCase.AccountId = conInsertionList[0].AccountId;
            }
            caseInsertionList.add(objCase);
        }
        insert caseInsertionList;

        // Create a dummy case record for negative testing
        Case objDummyCase = new Case();
        objDummyCase.Subject = 'Dummy Case Subject';
        objDummyCase.Description = 'Dummy Case Description';
        objDummyCase.recordtypeid = RecordTypeIdCase;
        objDummyCase.Status = 'New';
        objDummyCase.Origin = 'Email';
        objDummyCase.Date_Required__c = Date.today();
        objDummyCase.ContactId = conInsertionList[0].Id;
        insert objDummyCase;
        
        
        
        // Insert ContentVersion
        for(Integer i=0; i < contentVerRecCount; i++) {
            ContentVersion objContentVersion = new ContentVersion();
            objContentVersion.Title = 'Test Title';
            objContentVersion.PathOnClient = 'Test.jpg';
            objContentVersion.VersionData = Blob.valueOf('Test Content Data');
            objContentVersion.IsMajorVersion = true;
            contentVerInsertionList.add(objContentVersion);
        }
        insert contentVerInsertionList;
        
        // Fetch Content Document records
        List<ContentDocument> documents = [SELECT Id, 
                                                  Title, 
                                                  LatestPublishedVersionId 
                                             FROM ContentDocument];
        //System.debug('documents======='+documents);  

        // Insert ContentDocumentLink
        for(Integer i=0; i < contentDocLinkRecCount; i++) {
            ContentDocumentLink objContentDocLink = new ContentDocumentLink();
            if(caseInsertionList.size() >= i) {
                objContentDocLink.LinkedEntityId = caseInsertionList[i].Id;
            } 
            if(documents.size() >= i) {
                objContentDocLink.ContentDocumentId = documents[i].Id;
            } 
            objContentDocLink.Visibility = 'AllUsers';
            objContentDocLink.ShareType= 'V';
            contentDocLinkInsertionList.add(objContentDocLink);
        }  
        insert contentDocLinkInsertionList; 
        
        // Insert Case Monitoring
        for(Integer i=0; i < caseMonRecCount; i++) {
            Case_Monitoring__c objCaseMonitor = new Case_Monitoring__c();
            if(caseInsertionList.size() >= i) {
                objCaseMonitor.Case__c = caseInsertionList[i].Id;
            }
            objCaseMonitor.Communication_Type__c = 'File';
            if(contentVerInsertionList.size() >= i) {
                objCaseMonitor.Parent_ID__c = contentVerInsertionList[i].Id; 
            }
            objCaseMonitor.Review_Complete__c = false;
            caseMonInsertionList.add(objCaseMonitor);
        }
        insert caseMonInsertionList;

        // Insert Case Monitoring record without ParentId to test negative scenarios
        Case_Monitoring__c objCaseMonitorNegative = new Case_Monitoring__c();
        objCaseMonitorNegative.Case__c = caseInsertionList[0].Id;
        objCaseMonitorNegative.Review_Complete__c = false;
        insert objCaseMonitorNegative;                                  
        
    }
    
    /**
    * Method Name : getFilesRecordsPositiveTest
    * Parameters  : none
    * Description : This method is used to test positive scenario of getFilesRecords functionality
    **/
    @isTest
    public static void getFilesRecordsPositiveTest() {
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        //System.debug('listCase======='+listCase+'==========listCase size======='+listCase.size());
        List<FilesController.FileWrapper> listFileWrapper = new List<FilesController.FileWrapper>();
        if(!listCase.isEmpty()) {
            Test.startTest();
            listFileWrapper = FilesController.getFilesRecords(listCase[0].Id);
            Test.stopTest();
        }
        // Assert : Check if FileWrapper list that is returned from the method is not null
        System.assert(!listFileWrapper.isEmpty(),'File Wrapper list is empty');
    }
    /**
    * Method Name : getFilesRecordsNegativeTest
    * Parameters  : none
    * Description : This method is used to test negative scenario of getFilesRecords functionality
    **/
    @isTest
    public static void getFilesRecordsNegativeTest() {

        // Fetch dummy case without any linked content document
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case
                     WHERE Subject = 'Dummy Case Subject'];
        //System.debug('listCase======='+listCase+'==========listCase size======='+listCase.size());
        List<FilesController.FileWrapper> listFileWrapper = new List<FilesController.FileWrapper>();
        if(!listCase.isEmpty()) {
            Test.startTest();
            listFileWrapper = FilesController.getFilesRecords(listCase[0].Id);
            Test.stopTest();
        }
        // Assert : Check if FileWrapper list that is returned from the method is not null
        System.assert(listFileWrapper.isEmpty(),'File Wrapper list is not empty');
    }
    /**
    * Method Name : getFilesPositiveTest
    * Parameters  : none
    * Description : This method is used to test positive scenario of getFiles functionality
    **/
    @isTest
    public static void getFilesPositiveTest() {
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        if(!listCase.isEmpty()) {
            Test.startTest();
            objFilesControllerWrapper = FilesController.getFiles(listCase[0].Id);
            Test.stopTest();
        }
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper != null,'File Controller Wrapper object is null');
    }
    /**
    * Method Name : getFilesNegativeTest
    * Parameters  : none
    * Description : This method is used to test negative scenario of getFiles functionality
    **/
    @isTest
    public static void getFilesNegativeTest() {
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case
                     WHERE Subject = 'Dummy Case Subject'];
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        if(!listCase.isEmpty()) {
            Test.startTest();
            objFilesControllerWrapper = FilesController.getFiles(listCase[0].Id);
            Test.stopTest();
        }
        //System.debug('objFilesControllerWrapper::::::::===='+objFilesControllerWrapper);
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper.files.size() == 0,'Files list in Files Controller Wrapper is not empty');
    }
    
    /**
    * Method Name : saveRecordCmRecordPositiveTest
    * Parameters  : none
    * Description : This method is used to test positive scenario of saveRecordCmRecord functionality
    **/
    @isTest
    public static void saveRecordCmRecordPositiveTest() {
    
        // Fetch case monitoring record so as to pass it as method parameter
        List<Case_Monitoring__c> listCaseMonitor = new List<Case_Monitoring__c>();
        listCaseMonitor = [SELECT Id, Parent_ID__c, Review_Complete__c
                      FROM Case_Monitoring__c];
        listCaseMonitor[0].Review_Complete__c = true;
        String cmRecordJSONStr = JSON.serialize(listCaseMonitor[0]);
        if(String.isNotBlank(cmRecordJSONStr)) {
            Test.startTest();
            FilesController.saveRecordCmRecord(cmRecordJSONStr);
            Test.stopTest();
        }
        // Get the content version record
        List<ContentVersion> cvList = [SELECT Id, 
                                              Review_Complete__c 
                                         FROM ContentVersion 
                                        WHERE IsLatest = True 
                                          AND Id = :listCaseMonitor[0].Parent_ID__c];
                                          
        // Assert :  Check if Review Complete checkbox for content version is set to true
        System.assert(cvList[0].Review_Complete__c == true,'Content version record is not updated');
    }
    /**
    * Method Name : saveRecordCmRecordNegativeTest
    * Parameters  : none
    * Description : This method is used to test negative scenario of saveRecordCmRecord functionality
    **/
    @isTest
    public static void saveRecordCmRecordNegativeTest() {
    
        // Fetch case monitoring record so as to pass it as method parameter
        List<Case_Monitoring__c> listCaseMonitor = new List<Case_Monitoring__c>();
        listCaseMonitor = [SELECT Id, Parent_ID__c, Review_Complete__c
                             FROM Case_Monitoring__c
                            WHERE Parent_ID__c = null];
        listCaseMonitor[0].Review_Complete__c = true;
        String cmRecordJSONStr = JSON.serialize(listCaseMonitor[0]);
        if(String.isNotBlank(cmRecordJSONStr)) {
            Test.startTest();
            FilesController.saveRecordCmRecord(cmRecordJSONStr);
            Test.stopTest();
        }
        // Get the content version record
        List<ContentVersion> cvList = [SELECT Id, 
                                              Review_Complete__c 
                                         FROM ContentVersion 
                                        WHERE IsLatest = true 
                                          AND Review_Complete__c = true];
                                          
        // Assert :  Check if Review Complete checkbox for content version is set to true
        System.assert(cvList.size() == 0,'Content version record is updated');
    }
    
    /**
    * Method Name : saveRecordPositiveTest
    * Parameters  : none
    * Description : This method is used to test the positive scenario of saveRecord functionality
    **/
    @isTest
    public static void saveRecordPositiveTest() {
        List<Case_Monitoring__c> listCaseMonitor = new List<Case_Monitoring__c>();
        listCaseMonitor = [SELECT Id, Parent_ID__c, Review_Complete__c
                      FROM Case_Monitoring__c];
        listCaseMonitor[0].Review_Complete__c = true;
        
        if(!listCaseMonitor.isEmpty()) {
            Test.startTest();
            FilesController.saveRecord(listCaseMonitor[0]);
            Test.stopTest();
            // Get the content version record
            List<ContentVersion> cvList = [SELECT Id, 
                                                  Review_Complete__c 
                                             FROM ContentVersion 
                                            WHERE IsLatest = True 
                                              AND Id = :listCaseMonitor[0].Parent_ID__c];
                                              
        // Assert :  Check if Review Complete checkbox for content version is set to true
        System.assert(cvList[0].Review_Complete__c == true,'Contetn version record is not updated');
        }
    }
    /**
    * Method Name : saveRecordNegativeTest
    * Parameters  : none
    * Description : This method is used to test the negative scenario of saveRecord functionality
    **/
    @isTest
    public static void saveRecordNegativeTest() {
        List<Case_Monitoring__c> listCaseMonitor = new List<Case_Monitoring__c>();
        listCaseMonitor = [SELECT Id, 
                                  Parent_ID__c, 
                                  Review_Complete__c
                             FROM Case_Monitoring__c
                            WHERE Parent_ID__c = null];
        listCaseMonitor[0].Review_Complete__c = true;
        
        if(!listCaseMonitor.isEmpty()) {
            Test.startTest();
            FilesController.saveRecord(listCaseMonitor[0]);
            Test.stopTest();
            // Get the content version record
            List<ContentVersion> cvList = [SELECT Id, 
                                                  Review_Complete__c 
                                             FROM ContentVersion 
                                            WHERE IsLatest = True 
                                              AND Id = :listCaseMonitor[0].Parent_ID__c];
                                              
        // Assert :  Check if Review Complete checkbox for content version is set to true
        System.assert(cvList.size() == 0,'Content version record is updated');
        }
    }
    
    /**
    * Method Name : getFilesClonePositiveTest
    * Parameters  : none
    * Description : This method is used to test the positive scenario of getFilesClone functionality
    **/
    @isTest
    public static void getFilesClonePositiveTest() {
        List<Case> listCase = new List<Case>();
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        listCase = [SELECT Id
                      FROM Case];
        //System.debug('listCase======='+listCase+'==========listCase size======='+listCase.size());
        if(!listCase.isEmpty()) {
            objFilesControllerWrapper = FilesController.getFilesClone(listCase[0].Id);
        }
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper.files.size() > 0,'Files list in Files Controller Wrapper is empty');
        
    }
    /**
    * Method Name : getFilesCloneNegativeTest
    * Parameters  : none
    * Description : This method is used to test the negative scenario of getFilesClone functionality
    **/
    @isTest
    public static void getFilesCloneNegativeTest() {

        List<Case> listCase = new List<Case>();
        FilesController.FilesControllerWrapper objFilesControllerWrapper = new FilesController.FilesControllerWrapper();
        listCase = [SELECT Id
                      FROM Case
                      WHERE Subject = 'Dummy Case Subject'];
        //System.debug('listCase======='+listCase+'==========listCase size======='+listCase.size());
        if(!listCase.isEmpty()) {
            objFilesControllerWrapper = FilesController.getFilesClone(listCase[0].Id);
        }
        // Assert : Check if returned result of method that is File Controller Wrapper object is not null
        System.assert(objFilesControllerWrapper.files.size() == 0,'Files list in Files Controller Wrapper is not empty');
        
    }
    
    /**
    * Method Name : getAllFilesPositiveTest
    * Parameters  : none
    * Description : This method is used to test the positive scenarios of getAllFiles functionality
    **/
    @isTest
    public static void getAllFilesPositiveTest() {
        // Fetch Content Document records
        List<ContentDocument> documents = [SELECT Id, 
                                                  Title, 
                                                  LatestPublishedVersionId 
                                             FROM ContentDocument];
                        
        Test.startTest();
        List<ContentVersion> listContentVersion = FilesController.getAllFiles(new List<String>{documents[0].Id});
        Test.stopTest();
        
        // Assert : Check if there are related content version records related to the document that 
        // is been passed as parameter to the metod
        System.assert(!listContentVersion.isEmpty(),'Content Version list is empty');
    }
    /**
    * Method Name : getAllFilesNegativeTest
    * Parameters  : none
    * Description : This method is used to test the negative scenarios of getAllFiles functionality
    **/
    @isTest
    public static void getAllFilesNegativeTest() {
        // Fetch Content Document records
        
        Test.startTest();
        // Pass dummy document Id to the method
        List<ContentVersion> listContentVersion = FilesController.getAllFiles(new List<String>{'069241516172818','069536474829281'});
        Test.stopTest();
        
        // Assert : Check if there are related content version records related to the document that 
        // is been passed as parameter to the metod
        System.assert(listContentVersion.isEmpty(),'Content Version list is not empty');
    }
    
    /**
    * Method Name : deleteFilePositiveTest
    * Parameters  : none
    * Description : This method is used to test the positive scenarios of deleteFile functionality
    **/
    @isTest
    public static void deleteFilePositiveTest() {
        // Fetch Content Document records
        List<ContentDocument> documents = [SELECT Id, 
                                                  Title, 
                                                  LatestPublishedVersionId 
                                             FROM ContentDocument];
        Id docId = documents[0].Id;
        Test.startTest();
        String getSuccessMsg = '';
        getSuccessMsg = FilesController.deleteFile(documents[0].Id);
        Test.stopTest();
        // Fetch content document record again after method call to check if content document record is deleted or not
        List<ContentDocument> contentDocuments = [SELECT Id, 
                                                         Title, 
                                                         LatestPublishedVersionId 
                                                    FROM ContentDocument
                                                   WHERE Id = :docId];
        // Assert : Check if content document record is deleted
        System.assert(contentDocuments.size() == 0,'Content Document record is not deleted');  
        // Assert : Check if content document record is deleted
        System.assert(getSuccessMsg.equals('File was deleted.'),'File is not deleted');  
    }
    
    /**
    * Method Name : saveFilesTest
    * Parameters  : none
    * Description : This method is used to test saveFiles functionality
    **/
    @isTest
    public static void saveFilesTest() {
        // Fetch Content Document records
        
        List<Case> listCase = new List<Case>();
        List<ContentVersion> listContentVersion = new List<ContentVersion>();
        List<ContentVersion> listContentVersionUpdated = new List<ContentVersion>();
        listCase = [SELECT Id
                      FROM Case];
        listContentVersion = [SELECT Id,
                                     Review_Complete__c
                                FROM ContentVersion
                               LIMIT 1];
       Id contentVerId = listContentVersion[0].Id;
       if(!listContentVersion.isEmpty() && !listCase.isEmpty()) {
            listContentVersion[0].Review_Complete__c = true;
            FilesController.saveFiles(listContentVersion, listCase[0].Id);
            listContentVersionUpdated = [SELECT Id,
                                                Review_Complete__c
                                           FROM ContentVersion
                                          WHERE Id = :contentVerId];
            // Assert :  Check if content version is updated 
            System.assert(listContentVersionUpdated[0].Review_Complete__c == true,'Content Version is updated');  
       }
        
        
    }
    /**
    * Method Name : updateCaseRecordTest
    * Parameters  : none
    * Description : This method is used to test updateCaseRecord functionality
    **/
    @isTest
    public static void updateCaseRecordTest() {
        String parentId;
        list<String> fileIds = new List<String>();
        Boolean isCommunityUser;
        Test.startTest();
        FilesController.updateCaseRecord(parentId, fileIds, isCommunityUser);
        Test.stopTest();
    }
}
@isTest
public class AdjustmentController_Test 
{
    @isTest static void TestSave()
    {
     	Account objAccount = new Account();
        objAccount.name = 'some';
        insert objAccount;
                  
      	//Contact objcontact = [Select Id, AccountId	 From Contact where lastname = 'Admin'];
      	//List<Contact> objContactList = [Select Id, AccountId From Contact];
      	//
      	Contact objContact = new Contact();
        objContact.LastName = 'xxx';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'mmclean@synergentcorp.com';
        insert objContact;
        Id objContactId = objContact.Id;
		
         
        Case objNewCase = new Case();
        objNewCase.Subject = 'ECC Form Submission';
        objNewCase.Description = 'Form submitted thru community portal.';
        objNewCase.Service_Area__c = 'Check Processing';
        objNewCase.Category__c = 'Adjustment';
        objNewCase.Sub_Category__c = 'Other';
        
        objNewCase.Status = 'New';
        objNewCase.Type = 'Customer Service Request';
        // maybe not 
        objNewCase.Origin = 'Synergent Case Portal';
        objNewCase.Product_Type__c = 'Check_Processing';
        objNewCase.AccountId = objAccount.Id;
        objNewCase.ContactId = objContactId;
                
        Id idRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('External Service Request').getRecordTypeId();
        objNewCase.RecordTypeId = idRecordTypeId;
        insert objNewCase;
		ID objCaseId = objNewCase.Id;
        
        
        Adjustment__c obj = new Adjustment__c();
        obj.Case__c = objNewCase.Id;
        
        

        
        Adjustment__c objzzzz = AdjustmentController.Save(obj, objCaseId,objContactId);
       
        
    }

     @isTest static void TestGet()
     {
        Adjustment__c obj = new Adjustment__c();
        insert obj;
        Adjustment__c objzzzz = AdjustmentController.GetAdjustment(obj.Id);
     }
}